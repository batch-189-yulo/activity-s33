//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos')

	.then((response) => response.json())
	.then((data) => console.log(data))

//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.


let todo_titles

fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response) => response.json())
	.then((data) => {
		todo_titles = data.map((todo => {
					return todo.title;
		}))
	})
	.then(() => console.log(todo_titles))


//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/2')

	.then((response) => response.json())
	.then((data) => console.log(data))


//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.


fetch('https://jsonplaceholder.typicode.com/todos/2')

	.then((response) => response.json())
	.then((data) => console.log(`The item "${data.title}" on the list has a status of ${data.completed}.`));



//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'New Post',
		body: 'I am the new post'
	})
})
	.then((response) => response.json())
	.then((data) => console.log(data))


//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API
fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated to do list'
	})
})
	.then((response) => response.json())
	.then((data) => console.log(data))


//9. update to do list by changing the data structure.
let today = new Date();
let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
let dateTime = date+' '+time;


fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'odit optio omnis qui sunt',
		description: 'the description',
		status: 'Completed',
		dateCompleted: `${dateTime}`,
		userId : 4

	})
})
	.then((response) => response.json())
	.then((data) => console.log(data))


//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'this item has been patched'
	})
})
	.then((response) => response.json())
	.then((data) => console.log(data))

//11. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: true,
        dateCompleted: new Date()
    })
})
.then((response) => response.json())
.then((data) => console.log(data));

//12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'DELETE'
})
	.then((response) => response.json())
	.then((data) => console.log(data))



